package com.retail.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.retail.com.Item;
import com.retail.factory.ShippingFactory;

public class GroundShippingTest {

	private ShippingCost sc;
	private ShippingFactory sf;
	Item i = new Item();

	@Before
	public void setUp() {
		sf = new ShippingFactory();
		sc = sf.getShippingCost("GROUND");
	}

	@Test
	public void testCalculateCost() {
		i.setUpc(567321101986L);
		i.setDescription("CD � Beatles, Abbey Road");
		i.setPrice(17.99);
		i.setWeight(0.61);
		i.setShippingMethod("GROUND");
		double expectedCost = 1.53;
		double actualCost = sc.calculateCost(i);

		assertEquals(actualCost, expectedCost, 001);
	}

}
