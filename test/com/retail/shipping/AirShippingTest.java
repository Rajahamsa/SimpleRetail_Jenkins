package com.retail.shipping;



import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.retail.com.Item;
import com.retail.factory.ShippingFactory;

public class AirShippingTest {
	private ShippingCost sc;
	private ShippingFactory sf;
	Item i=new Item();
	
	@Before
	public void setUp(){
	sf=new ShippingFactory();
	sc=sf.getShippingCost("AIR");
	}

	@Test
	public void testCalculateCost() {
		i.setUpc(567321101987L);
		i.setDescription("CD � Pink Floyd, Dark Side Of The Moon");
		i.setPrice(19.99);
		i.setWeight(0.58);
		i.setShippingMethod("AIR");
		double expectedCost=4.64;
		double actualCost=sc.calculateCost(i);
		assertEquals(expectedCost,actualCost,001);
	}

}
