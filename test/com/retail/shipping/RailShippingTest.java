package com.retail.shipping;



import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.retail.com.Item;
import com.retail.factory.ShippingFactory;

public class RailShippingTest {
	private ShippingCost sc;
	private ShippingFactory sf;
	Item i=new Item();
	
	@Before
	public void setUp(){
	sf=new ShippingFactory();
	sc=sf.getShippingCost("RAIL");
	}

	@Test
	public void testLess() {
		i.setUpc(322322202488L);
		i.setDescription("HeavyMac Laptop");
		i.setPrice(4555.79);
		i.setWeight(4.08);
		i.setShippingMethod("RAIL");
		double expectedCost=5;
		double actualCost=sc.calculateCost(i);
		assertEquals(expectedCost,actualCost,001);
	}
	@Test
	public void testGreater() {
		i.setUpc(312321101516L);
		i.setDescription("Hot Tub");
		i.setPrice(9899.99);
		i.setWeight(793.41);
		i.setShippingMethod("RAIL");
		double expectedCost=10;
		double actualCost=sc.calculateCost(i);
		assertEquals(expectedCost,actualCost,001);
	}

}
