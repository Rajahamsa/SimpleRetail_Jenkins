package com.retail.test;

import java.util.Set;

import com.retail.com.Item;
import com.retail.com.ItemManager;
import com.retail.report.Batch;


public class Test {

	public static void main(String[] args) {
		ItemManager im = new ItemManager();
		Set<Item> itemSet = im.initialize();

		Batch bt = new Batch();
		bt.batchItems(itemSet);

	}


}