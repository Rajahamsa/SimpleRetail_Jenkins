package com.retail.shipping;

import com.retail.com.Item;

public class RailShipping implements ShippingCost {

	@Override
	public double calculateCost(Item i) {
		double cost = 0;
		if (i.getWeight() < 5) {
			cost = 5;
		} else {
			cost = 10;
		}
		return cost;
	}

}
