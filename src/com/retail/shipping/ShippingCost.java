package com.retail.shipping;

import com.retail.com.Item;

public interface ShippingCost {
	public double calculateCost(Item i);
}
