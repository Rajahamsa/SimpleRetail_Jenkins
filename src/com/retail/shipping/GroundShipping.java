package com.retail.shipping;

import com.retail.com.Item;

public class GroundShipping implements ShippingCost {

	@Override
	public double calculateCost(Item i) {
		double cost = i.getWeight() * 2.5;
		return cost;
	}

}
