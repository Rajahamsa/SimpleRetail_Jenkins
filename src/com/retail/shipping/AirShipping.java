package com.retail.shipping;

import com.retail.com.Item;

public class AirShipping implements ShippingCost {

	@Override
	public double calculateCost(Item i) {
		long l = i.getUpc();
		String upc = Long.toString(l);
		int lsdigit = Integer.parseInt(upc.charAt(upc.length() - 2) + "");
		double cost = i.getWeight() * lsdigit;
		return cost;
	}

}
