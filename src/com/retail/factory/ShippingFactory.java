package com.retail.factory;

import com.retail.shipping.AirShipping;
import com.retail.shipping.GroundShipping;
import com.retail.shipping.RailShipping;
import com.retail.shipping.ShippingCost;

public class ShippingFactory {

	// use getShippingCost method to get object of type shipping
	public ShippingCost getShippingCost(String ShippingType) {

		if ("AIR".equalsIgnoreCase(ShippingType)) {
			return new AirShipping();

		} else if ("GROUND".equalsIgnoreCase(ShippingType)) {
			return new GroundShipping();

		} else if ("RAIL".equalsIgnoreCase(ShippingType)) {
			return new RailShipping();
		}
		return null;
	}
}
