package com.retail.report;

import java.util.Date;
import java.util.Set;

import com.retail.com.Item;
import com.retail.factory.ShippingFactory;
import com.retail.shipping.ShippingCost;

public class Report {

	public void displayShipmentReport(Set<Item> itemSet) {
        
		ShippingFactory sf=new ShippingFactory();
		ShippingCost sc;
		double cost = 0.0;
		double totalCost = 0.0;

		System.out.println("****SHIPMENT REPORT****" + "\t" + "\t" + "\t" + "Date:" + new Date());
		System.out.println();
		System.out.format("%13s %50s %15s %15s %10s %8s", "UPC", "Description", "Price", "Weight", "Shipping Method",
				"cost");
		System.out.println();

		for (Item i : itemSet) {
			if (i.getShippingMethod() == "AIR") {
				sc = sf.getShippingCost("AIR");
				cost = sc.calculateCost(i);
			} else {
				sc =sf.getShippingCost("GROUND");
				cost = sc.calculateCost(i);
			}
			totalCost = totalCost + cost;
			System.out.format("%13s %50s %15s %15s %10s %13s", i.getUpc(), i.getDescription(), i.getPrice(),
					i.getWeight(), i.getShippingMethod(), String.format("%.2f", cost));
			System.out.println();

		}
		System.out.println();
		System.out.println("TOTAL SHIPPING COST:" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t"
				+ "\t" + "\t" + "    " + totalCost);
	}

}
