package com.retail.report;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.retail.com.Item;
import com.retail.com.ItemComparator;

public class Batch {

	public void batchItems(Set<Item> itemSet) {
		Report report = new Report();
		List<Item> list = new ArrayList<Item>(itemSet);
		int size = itemSet.size();
		int chuncks = size / 6;
		int j = 6;
		for (int i = 0; i < chuncks; i++) {
			if (i == 0) {
				Set<Item> subset1 = new TreeSet<Item>(new ItemComparator());
				subset1.addAll(list.subList(0, 6 * i + j));
				report.displayShipmentReport(subset1);
			}
			if (i > 0 && i < chuncks) {
				Set<Item> subset2 = new TreeSet<Item>(new ItemComparator());
				subset2.addAll(list.subList(6 * i, 6 * i + j));
				report.displayShipmentReport(subset2);
				subset2.removeAll(subset2);
			}
		}

	}

}
