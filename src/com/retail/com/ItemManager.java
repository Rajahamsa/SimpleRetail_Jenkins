package com.retail.com;

import java.util.Set;
import java.util.TreeSet;

public class ItemManager {

	public Set<Item> initialize() {
		Set<Item> Itemset = new TreeSet<Item>(new ItemComparator());

		Item i = new Item();
		i.setUpc(567321101987L);
		i.setDescription("CD � Pink Floyd, Dark Side Of The Moon");
		i.setPrice(19.99);
		i.setWeight(0.58);
		i.setShippingMethod("AIR");

		Itemset.add(i);

		i = new Item();
		i.setUpc(567321101986L);
		i.setDescription("CD � Beatles, Abbey Road");
		i.setPrice(17.99);
		i.setWeight(0.61);
		i.setShippingMethod("GROUND");

		Itemset.add(i);

		i = new Item();
		i.setUpc(567321101985L);
		i.setDescription("CD � Queen, A Night at the Opera");
		i.setPrice(20.49);
		i.setWeight(0.55);
		i.setShippingMethod("AIR");

		Itemset.add(i);

		i = new Item();
		i.setUpc(567321101984L);
		i.setDescription("CD � Michael Jackson, Thriller");
		i.setPrice(23.88);
		i.setWeight(0.50);
		i.setShippingMethod("GROUND");

		Itemset.add(i);

		i = new Item();
		i.setUpc(467321101899L);
		i.setDescription("iPhone - Waterproof Case");
		i.setPrice(9.75);
		i.setWeight(0.73);
		i.setShippingMethod("AIR");

		Itemset.add(i);

		i = new Item();
		i.setUpc(477321101878L);
		i.setDescription("iPhone -  Headphones");
		i.setPrice(17.25);
		i.setWeight(3.21);
		i.setShippingMethod("GROUND");

		Itemset.add(i);

		i = new Item(123243432L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(123243436L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(123243437L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(123243438L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(123243439L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(123243430L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);

		i = new Item(223243432L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(323243436L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(423243437L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(523243438L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(623243439L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);
		i = new Item(723243430L, "DSDSADSA", 21.2, 54, "GROUND");
		Itemset.add(i);

		return Itemset;

	}

}
