package com.retail.com;

import java.util.Comparator;

public class ItemComparator implements Comparator<Item> {

	@Override
	public int compare(Item i1, Item i2) {
		if (i1.getUpc() > i2.getUpc()) {
			return 1;
		} else {
			return -1;
		}
	}
}
